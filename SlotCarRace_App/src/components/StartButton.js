import React from 'react';

import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome5';
import theme from '../theme/theme';

function StartButton() {
    return (
        <TouchableOpacity><Icon name='stop' size={25} color={theme.SECONDARY_COLOR}/></TouchableOpacity>
        // <Icon name='stop' size={25} color={theme.SECONDARY_COLOR}/>
    );
}

StartButton.propTypes = {

};

const styles = StyleSheet.create({

});

export default StartButton
