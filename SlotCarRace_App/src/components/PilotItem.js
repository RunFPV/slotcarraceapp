import React from 'react';

import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Proptypes from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome5';
import theme from '../theme/theme';


function PilotItem(props) {
    return (
        <TouchableOpacity style={styles.container}>
            <Text style={styles.place}>#{props.pilot.place}</Text>
            <View>
                <Text style={styles.name}>{props.pilot.name}</Text>
                <Text>BL: {props.pilot.bestLap}</Text>
                <Text>LL: {props.pilot.lastLap}</Text>
            </View>
            <View>
                <Text>{props.pilot.laps} laps</Text>
                <Text>{props.pilot.gap}</Text>
            </View>
        </TouchableOpacity>
    );
}

PilotItem.propTypes = {
    pilot: Proptypes.object.isRequired,
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    place: {
        fontSize: theme.FONT_SIZE_XLARGE,
    },
    name: {
        fontSize: theme.FONT_SIZE_LARGE,
    }
});

export default PilotItem;




